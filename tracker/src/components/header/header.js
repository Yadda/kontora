import './header.css';
import { NavLink } from 'react-router-dom'

function App() {
  return (
    <div className="Header">
      <NavLink to="/">
        <div className="Header__logo">
          Контора<br/>{"П&∂@#%$*в"}
        </div>
      </NavLink>
      <NavLink to="/companies">
        <button className="Header__link">Для компаний</button>
      </NavLink>
    </div>
  );
}

export default App;
